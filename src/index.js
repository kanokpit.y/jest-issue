const express = require('express');

const app = express();
const port = 5000 || process.env.PORT;

app.get('/', (req, res) => {
    res.status(200).json({ message: 'Hello World of CI/CD' })
})

app.listen(port, () => {
    console.log(`App started on port ${port}`)
});

module.exports = app;
